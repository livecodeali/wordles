# Wordles of the world, unite!

This is the source code (Python + YAML) for generating a list of Wordle-like games (https://rwmpelstilzchen.gitlab.io/wordles/).


## How to contribute

See the [boost](https://rwmpelstilzchen.gitlab.io/wordles/#boost) section on the project’s page.


## Database structure

The database is made of [YAML](https://en.wikipedia.org/wiki/YAML) files. Each is used by one table on the page, with the exception of `database/languages.yaml`, holds information that is used by several tables.


### `languages.yaml`

- **`iso`** 🔑: Language identification code. If the language has an [ISO 639-3](https://en.wikipedia.org/wiki/ISO_639-3) code, use it. If not, use [Glottolog](https://glottolog.org/)’s codes (with `Glottolog-` prefix). The third best option is [IETF](https://en.wikipedia.org/wiki/IETF_language_tag) language tags (`IETF-` prefix). If none is available, use `nocode-` prefix and make your own identifier. `multilingual` is used in multilingual entries.

- **`native`**: Information about the language in the language itself.
	- **`name`**: The name of the language.
	- **`wiki`** (optional): Wikipedia article.
		- **`title`**: The article’s title.
		- **`lang`**: Language code on Wikipedia (see the URL: `https://XX.wikipedia.org/wiki/ARTICLE_NAME`).

- **`english`**: Information about the language in English.
	- **`name`**: The name of the language.
	- **`wiki`** (optional): Wikipedia article’s title.

### Table files

(WIP)

- **`name`**: The name of the game.
- **`lang`**: Language of the game. Used in `mlwordles.yaml`, `domain.yaml` and `twist.yaml`
- **`url`**: The URL of the game.
- **`src`** (optional): The URL of the source-code repository.
- **`src-type`** (if there is a `src` field): Type of repository (`github`, `gitlab`, `git` (generic), `glitch`, `pico-8`, or `other`).
- **`announcement`** (optional; currently has no effect on the output): Information regarding the announcement on social media (usually Twitter).
	- **`date`**: In YYYY-MM-DD format.
	- **`url`**.
- **`developer`** (optional; currently has no effect on the output): Information about the developer.
	- **`name`**.
	- **`url`**: Personal website, GitHub page, Twitter account, etc.
- **`reference`** (optional; has no effect on the output): Where did you hear about the game.
